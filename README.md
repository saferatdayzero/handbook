# Safer@DayZero site

<!-- BADGIE TIME -->

[![pipeline status](https://img.shields.io/gitlab/pipeline-status/ul-dsri/saferatdayzero/www?branch=main)](https://gitlab.com/ul-dsri/saferatdayzero/www/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![cici-tools enabled](https://img.shields.io/badge/%E2%9A%A1_cici--tools-enabled-c0ff33)](https://gitlab.com/buildgarden/tools/cici-tools)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

Source project for the [Safer@DayZero site](https://saferatdayzero.dev).

> Safer@DayZero is a research project at a very early stage of development.
>
> The contents of this site are made available publicly solely to facilitate
> collaboration with research partners. These resources:
>
> -   are published without warranty,
> -   are subject to change at any time, and
> -   have not been certified, tested, assessed, or otherwise assured of safety by
>     any person or organization.
>
> **Use at your own risk.**

## Setup

Set up a virtual environment

```bash
make setup
```

## License

Copyright 2024 UL Research Institutes.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
