# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

.PHONY: all
all: serve

VENV = venv
MKDOCS = $(VENV)/bin/mkdocs

$(VENV):
	uv venv venv

.PHONY: setup
setup: $(VENV)/requirements.txt

requirements.txt: requirements.in
	uv pip compile -o requirements.txt requirements.in

$(VENV)/requirements.txt: requirements.txt | $(VENV)
	VIRTUAL_ENV=$(VENV) uv pip install -r requirements.txt
	touch $(VENV)/requirements.txt

.PHONY: build
build:
	$(MKDOCS) build

.PHONY: serve
serve:
	$(MKDOCS) serve

.PHONY: clean
clean:
	rm -rf site/ public/
