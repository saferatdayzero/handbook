# BuildGarden

[BuildGarden](https://gitlab.com/buildgarden/) is a collection of reference
CI/CD implementations.

BuildGarden can be thought of as a proving ground. The Safer@DayZero team uses
it to test ideas and answer questions about project structure, conventions,
tooling, services, and so forth.

This document will attempt to summarize how these projects work, how they are
organized, and how you can use them.

## Principles of operation

A BuildGarden pipeline is just a CI/CD pipeline, which is a thing that
implements workflow automation to solve some problem, such as build, test, and
release a software artifact.

What makes a _BuildGarden_ pipeline is the fact that it conforms to the
following criteria:

-   Jobs require little or no configuration when projects follow tool norms.

-   Pipelines do not mutate the project. A human operator is always required for
    source changes.

-   Pipelines are released often and semantically versioned.

-   Project versions are injected at build time, not tracked in source.

-   Pipeline projects implement minimal viable reference projects to test against.

-   Pipeline are run on a regular schedule to detect drift.

-   Linters are executed locally prior to commit and again as part of CI:

    -   For local Git hooks, [pre-commit](https://pre-commit.com/) is supported.

    -   For CI automation, [GitLab CI](https://docs.gitlab.com/ee/ci/) is
        supported.

-   Projects are standalone and can be meaningfully developed in isolation by
    third parties.

## State of practice

In its current incarnation, BuildGarden pipelines only exist for GitLab CI/CD.

### GitLab CI/CD implementation

BuildGarden pipelines for GitLab CI/CD observe the following properties.

1.  Project names have the following convention:

    ```
    ^[a-z][a-z0-9]*(-[a-z0-9])*$
    ```

    For example: `python`, `helm`, `container-registry`.

1.  Jobs names have the following convention:

    ```
    ^<project>(-[a-z0-9])*$
    ```

    Where `<project>` is the kebab case project name. For example: `python-twine`,
    `container-build`, `prettier`.

1.  Environment variable names have the following convention:

    ```
    ^<project>(_[A-Z0-9])*$
    ```

    Where `<project>` is the capital snake case project name. For example:
    `PYTHON_REPOSITORY_URL`, `CONTAINER_PROXY`, `ANSIBLE_PLAYBOOK`.

1.  Default values for environment variables are provided as global variables.

1.  There are three pipeline stages named:

    -   `test`

    -   `build`

    -   `deploy`

    The names are only used to implement job ordering.

1.  Container images from Docker Hub are prefixed with $CONTAINER_PROXY.

1.  Linter jobs disable caching, artifacts, and Git submodules.

1.  Pipeline projects are organized around singular top-level tools, and may
    contain jobs linters, build tools, security scanning, deployment, packaging,
    release, or anything else related to the top-level tool.

1.  Jobs are composable.

1.  Jobs do not cause side effects to other jobs when sourced concurrently.

1.  Multiple CI files can be sourced simultaneously from a project:

    ```yaml
    include:
        - project: buildgarden/pipelines/project
          files:
              - project-job1.yaml
              - project-job2.yaml
    ```

## What pipelines are available?

BuildGarden pipelines can be found at:

<https://gitlab.com/buildgarden/pipelines>

## How to adopt BuildGarden pipelines?

BuildGarden pipelines are not monolithic, as different organizations follow
different norms, and projects require varying amounts of work to adopt
BuildGarden pipeline features.

With this in mind, each pipeline contains a summary of what features it
contains, and how to include them.

Using the [Python pipeline](https://gitlab.com/buildgarden/pipelines/python) as
an example, the following jobs are supported at the time of writing:

```yaml
include:
    - project: buildgarden/pipelines/python
      file:
          - python-black.yml
          - python-isort.yml
          - python-mypy.yml
          - python-pyroma.yml
          - python-pytest.yml
          - python-setuptools.yml
          - python-twine.yml
          - python-vulture.yml
```

To add these to an existing project, a good rule of thumb is to add one linter
at a time. You’ll want to use pre-commit for this.

## pre-commit hooks

When you first clone your project (or when re-installing pre-commit) run:

```bash
pre-commit install
```

Periodically, to update pre-commit hooks to the latest versions, run:

```bash
pre-commit autoupdate
```

To force pre-commit to run on all files:

```bash
pre-commit run --all
```

At all other times, pre-commit will run automatically on relevant files that
have changed.
