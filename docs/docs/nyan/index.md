# Nyan

Nyan is the pipeline package index and data service powering the Safer@DayZero
ecosystem.

The source code for Nyan is hosted at
[gitlab.com/saferatdayzero/nyan](https://gitlab.com/saferatdayzero/nyan).

## Key concepts

Nyan is an attempt to expand the scope of the work that started with the
[Badgie](https://gitlab.com/buildgarden/tools/badgie) project.

### Data ingestion

```mermaid
flowchart LR
    github["GitHub"]
    gitlab["GitLab"]

    cici["Cici"]
    badgie["Badgie"]
    dataset["Dataset"]

    subgraph nyan["Nyan (nyan.saferatdayzero.dev)"]
        db[("PostgreSQL")]

        app["Application"]

        collector1[["Collector"]]
        collector2[["Collector"]]
        exporter[["Exporter"]]
    end

    app <--> db
    collector1 --> db
    collector2 --> db
    db --> exporter

    github --> collector1
    gitlab --> collector2

    sqlite["SQLite"]

    exporter --> sqlite
    sqlite --> cici
    sqlite --> badgie
    sqlite --> dataset
```

### Global namespace of tokens

Nyan provides a global namespace for tracking unique "tokens".

These tokens, at different times, correspond to package names, project names,
dev tools, languages, frameworks, and more. Example tokens might include:

-   `django`

-   `gitlab`

-   `python`

-   `python/black`

But they represent the idea in the _abstract_, not a specific implementation.

When a token is identified in a project by [Cici](../cici/index.md), a query is
made to the Nyan dataset to resolve an implementation of that token. Because
contacting a public service would be slow, we've chosen a SQL backend for Nyan
so that the Nyan database can be repackaged as a SQLite database and distributed
to local tools, making Nyan a "feeder team" for static analysis tools.

### Assignment authority

Nyan is the assignment authority for “tokens”.

## Data model

### Tokens

-   **Token** - A global registry of tokens.

    metadata - ID, title

### Projects

-   **Host** - where are the projects homed? GitHub, GitLab, KDE, Arch

    metadata - ID, title, url, description, maintainers, underlying software,
    credentials, OIDC, created_at, updated_at, ...

-   **Project** - source projects in an upstream

    metadata - ID, title, url, slug, organization (ID, not foreign key),
    created_at, updated_at, ...

-   **Snapshot** - tarball snapshots of the downloaded projects

    metadata - ID, md5_hash, created_at, updated_at

```mermaid
erDiagram
  Host {
    string title
    string url
    string description
  }
  Project {
    string version_control_system
    string hash
    string algorithm
  }
  Snapshot {
    string version
    string clone_url
    string commit_sha1
    string file_sha256
  }
  Snapshot }|--|| Project : snapshots
  Project }|--|| Host : projects
```

## Notes

If a project is renamed, forked, etc., we need to track those forks to the
extent that they are following a different development timeline.

Exclude _forks_ that do not have a community following.

Exclude projects that have a user base smaller than some threshold.

Track:

-   Languages used

-   Tools used

-   Files present maybe

-   Snapshots of the scanned projects

-   Observed config files

Possible tools that can help here:

-   GitHub linguist

-   pre-commit identify

-   cici

-   badgie

-   SCA scanners

-   vulnerability scanners

-   configuration scanners

-   Direct submissions

Tools that Nyan is similar to:

-   <https://artifacthub.io>

-   <https://pypi.org/>

-   <https://github.com/sindresorhus/awesome>

-   <https://libraries.io/>

-   <https://security.snyk.io/>

-   <https://tidelift.com/solutions/validated-open-source-package-intelligence>

-   <https://www.pepy.tech/>
