# Safer@DayZero

Safer@DayZero consists of the following components:

-   [BuildGarden](buildgarden/index.md) reference implementations.

-   [Cici](cici/index.md) pipeline manager.

-   [Nyan](nyan/index.md) pipeline package service.
